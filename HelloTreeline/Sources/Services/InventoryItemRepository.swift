//
//  InventoryItemRepository.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol InventoryItemRepository: AnyObject {
    func getAll(completion: @escaping (Result<[LocalInventoryItem], Error>) -> Void)
    func get(objectId: String) -> LocalInventoryItem?
    
    func addSellOfItem(with objectId: String)
    func substractSellOfItem(with objectId: String)
    
    func getSoldItems() -> [LocalInventoryItem]
    
    var delegate: InventoryItemRepositoryDelegate? {get set}
}

protocol InventoryItemRepositoryDelegate: AnyObject {
    func itemUpdated(item: LocalInventoryItem)
}

class DefaultInventoryItemRepository: InventoryItemRepository {
    
    static let shared: InventoryItemRepository = DefaultInventoryItemRepository()
    
    private let apiClient: APIClient
    
    private var items: [LocalInventoryItem] = []
    
    private var soldItems: [LocalInventoryItem] = []
    
    weak var delegate: InventoryItemRepositoryDelegate?
    
    init(apiClient: APIClient = DefaultAPIClient.shared) {
        self.apiClient = apiClient
    }
    
    func getAll(completion: @escaping (Result<[LocalInventoryItem], Error>) -> Void) {
        apiClient.get(endpoint: "getInventory", type: [InventoryItem].self) { result in
            DispatchQueue.main.async {
                switch result {
                case .success(let items):
                    let tempItems = self.items
                    self.items = items.map {LocalInventoryItem(with: $0)}
                    self.items.forEach {$0.itemsSold = tempItems.map { $0.id }.contains($0.id) ? $0.itemsSold : 0}
                    self.items.forEach { item in
                        item.itemsSold = tempItems.filter {$0.id == item.id}.first?.itemsSold ?? 0
                    }
                    completion(.success(self.items))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        }
    }
    
    func get(objectId: String) -> LocalInventoryItem? {
        items.first(where: { $0.id == objectId })
    }
    
    func addSellOfItem(with objectId: String) {
        guard let index = items.firstIndex(where: {$0.id == objectId}) else { return }
        items[index].addSellOfItem()
        delegate?.itemUpdated(item: items[index])
        
    }
    
    func substractSellOfItem(with objectId: String) {
        guard let index = items.firstIndex(where: {$0.id == objectId}) else { return }
        items[index].substractSellOfItem()
    }
    
    func getSoldItems() -> [LocalInventoryItem] {
        let solditems = items.filter { $0.itemsSold > 0 }
        return solditems
    }
}
