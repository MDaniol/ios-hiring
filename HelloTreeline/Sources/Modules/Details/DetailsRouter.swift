//
//  DetailsRouter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsRouter: AnyObject {
    func reportSales(for items: [LocalInventoryItem])
}

class DetailsDefaultRouter: DetailsRouter {

    private weak var viewController: DetailsDefaultViewController!

    public init(viewController: DetailsDefaultViewController) {
        self.viewController = viewController
    }
    
    func reportSales(for items: [LocalInventoryItem]) {
        let emailUrl = EmailComposer.composeEmailWithItems(items: items)
        if UIApplication.shared.canOpenURL(emailUrl) {
                UIApplication.shared.open(emailUrl, options: [:])
        }
    }

}
