//
//  DetailsViewContoller.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
import UIKit

protocol DetailsViewController: AnyObject {
    var navBarTitle: String? { get set }
    var idLabelTitle: String? { get set}
    var titleLabelTitle: String? { get set}
    var desciptionLabelTitle: String? { get set}
    var colorLabelTitle: String? { get set}
    var itemsAvailableLabelTitle: String? { get set}
    var itemsSoldLabelTitle: String? {get set}
    var reportSalesButtonTitle: String? {get set}
}

class DetailsDefaultViewController: UIViewController, DetailsViewController {
    
    var navBarTitle: String? {
        get { navigationItem.title }
        set { navigationItem.title = newValue }
    }
    
    var idLabelTitle: String? {
        get { idLabel.text }
        set { idLabel.text = newValue }
    }
    var titleLabelTitle: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    var desciptionLabelTitle: String? {
        get { desciptionLabel.text }
        set { desciptionLabel.text = newValue }
    }
    var colorLabelTitle: String? {
        get { colorLabel.text }
        set { colorLabel.text = newValue }
    }
    
    var itemsAvailableLabelTitle: String? {
        get { itemsAvailableLabel.text }
        set { itemsAvailableLabel.text = newValue }
    }
    
    var itemsSoldLabelTitle: String? {
        get { itemsSoldLabel.text }
        set { itemsSoldLabel.text = newValue }
    }
    
    var addSellButtonTitle: String? {
        get { addSellButton.title(for: .normal) }
        set { addSellButton.setTitle(newValue, for: .normal) }
    }
    
    var substractSellButtonTitle: String? {
        get { substractSellButton.title(for: .normal) }
        set { substractSellButton.setTitle(newValue, for: .normal) }
    }
    
    var reportSalesButtonTitle: String? {
        get { reportSalesButton.title(for: .normal) }
        set { reportSalesButton.setTitle(newValue, for: .normal) }
    }
    
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desciptionLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var itemsAvailableLabel: UILabel!
    @IBOutlet weak var itemsSoldLabel: UILabel!
    
    @IBOutlet weak var addSellButton: UIButton!
    @IBOutlet weak var substractSellButton: UIButton!
    @IBOutlet weak var reportSalesButton: UIButton!
    
    
    static func build(objectId: String) -> DetailsDefaultViewController {
        let viewController = UIStoryboard.main.instantiateViewController(of: DetailsDefaultViewController.self)!
        let router = DetailsDefaultRouter(viewController: viewController)
        let interactor = DetailsInteractor()
        
        viewController.presenter = DetailsPresenter(view: viewController, interactor: interactor, router: router, objectId: objectId)
        
        return viewController
    }
    
    private var presenter: DetailsPresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.viewDidLoad()
    }
    
    @IBAction func addSellItem(_ sender: Any) {
        presenter.addSellOfItem()
    }
    
    @IBAction func substractSellItem(_ sender: Any) {
        presenter.substractSellOfItem()
    }
    
    @IBAction func reportSales(_ sender: Any) {
        presenter.reportSales()
    }
}
