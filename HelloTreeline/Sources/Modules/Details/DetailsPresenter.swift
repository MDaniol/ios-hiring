//
//  DetailsPresenter.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

class DetailsPresenter: DetailsInteractorDelegate {

    private let router: DetailsRouter
    private let interactor: DetailsInteractor
    private weak var view: DetailsViewController!
    
    private let objectId: String

    init(view: DetailsViewController,
         interactor: DetailsInteractor,
         router: DetailsRouter,
         objectId: String) {
        
        self.view = view
        self.interactor = interactor
        self.router = router

        self.objectId = objectId
        
        self.interactor.delegate = self
    }
    
    
    func viewDidLoad() {
        interactor.loadItem(objectId: objectId)
    }
    
    private func updateUI(with item: LocalInventoryItem) {
        view.navBarTitle = item.title
        view.idLabelTitle = "ID: \(item.id)"
        view.titleLabelTitle = "Title: \(item.title)"
        view.desciptionLabelTitle = "Description: \(item.description)"
        view.colorLabelTitle = "Color: \(item.color)"
        view.itemsAvailableLabelTitle = "Items available: \(item.available)"
        view.itemsSoldLabelTitle = "Items sold: \(item.itemsSold)"
        view.reportSalesButtonTitle = "Report sales via Email"
    }
    
    func itemDidLoad(item: LocalInventoryItem) {
        updateUI(with: item)
    }
    
    func itemDidUpdated(item: LocalInventoryItem) {
        updateUI(with: item)
    }
    
    func addSellOfItem() {
        interactor.addSellOfItem(with: objectId)
    }
    
    func substractSellOfItem() {
        interactor.substractSellOfItem(with: objectId)
    }
    
    func reportSales() {
        let items = interactor.getSoldItems()
        router.reportSales(for: items)
    }
}
