//
//  EmailComposer.swift
//  HelloTreeline
//
//  Created by Mateusz Danioł on 06/12/2021.
//

import Foundation

final class EmailComposer {
    static func composeEmailWithItems(items: [LocalInventoryItem]) -> URL {
        let mailtoString = "mailto:bossman@bosscompany.com"
        let subjectString = "?subject=Sales Report"
        let bodyString = "&body=Todays sales report:\n\n\n"
        
        let salesItemsArray = items.map { item in
            return "\(item.id), \(item.type), \(item.color), \(item.title), SOLD: \(item.itemsSold)\n\n"
        }
        let salesItemsString = salesItemsArray.joined()
        
        let overallString = (mailtoString + subjectString + bodyString + salesItemsString).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let mailtoUrl = URL(string: overallString!)!
        return mailtoUrl
    }
}
