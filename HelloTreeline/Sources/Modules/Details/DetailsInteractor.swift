//
//  DetailsInteractor.swift
//  HelloTreeline
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation

protocol DetailsInteractorDelegate: AnyObject {
    func itemDidLoad(item: LocalInventoryItem)
    func itemDidUpdated(item: LocalInventoryItem)
}

class DetailsInteractor {

    weak var delegate: DetailsInteractorDelegate?
    
    private let itemRepository: InventoryItemRepository

    init(itemRepository: InventoryItemRepository = DefaultInventoryItemRepository.shared) {
        self.itemRepository = itemRepository
        self.itemRepository.delegate = self
    }
    
    func loadItem(objectId: String) {
        guard let item = itemRepository.get(objectId: objectId) else { return }
        delegate?.itemDidLoad(item: item)
    }
    
    func addSellOfItem(with objectId: String) {
        itemRepository.addSellOfItem(with: objectId)
    }
    
    func substractSellOfItem(with objectId: String) {
        itemRepository.substractSellOfItem(with: objectId)
    }
    
    func getSoldItems() -> [LocalInventoryItem] {
        return itemRepository.getSoldItems()
    }
}

extension DetailsInteractor: InventoryItemRepositoryDelegate {
    func itemUpdated(item: LocalInventoryItem) {
        delegate?.itemDidUpdated(item: item)
    }
}
