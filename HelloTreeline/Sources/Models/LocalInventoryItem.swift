//
//  LocalInventoryItem.swift
//  HelloTreeline
//
//  Created by Mateusz Danioł on 06/12/2021.
//

import Foundation

class LocalInventoryItem {
    
    let id: String
    let type: String
    let title: String
    let description: String
    let color: String
    let available: Int
    
    var itemsSold: Int
    
    init(with item: InventoryItem, sold: Int = 0) {
        self.id = item.id
        self.type = item.type
        self.title = item.title
        self.description = item.description
        self.color = item.color
        self.available = item.available
        
        self.itemsSold = sold
    }
    
    func addSellOfItem() {
        if itemsSold < available {
            itemsSold += 1
        }
    }
    
    func substractSellOfItem() {
        if itemsSold > 0 {
            itemsSold -= 1
        }
    }
}
