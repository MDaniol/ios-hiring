//
//  MockInventoryItemRepository.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import Foundation
@testable import HelloTreeline

class MockInventoryItemRepository: InventoryItemRepository {
    
    var delegate: InventoryItemRepositoryDelegate?
    
    func addSellOfItem(with objectId: String) {
        guard let index = items.firstIndex(where: {$0.id == objectId}) else { return }
        items[index].addSellOfItem()
        delegate?.itemUpdated(item: items[index])
    }
    
    func substractSellOfItem(with objectId: String) {
        guard let index = items.firstIndex(where: {$0.id == objectId}) else { return }
        items[index].substractSellOfItem()
        delegate?.itemUpdated(item: items[index])
    }
    
    var items: [LocalInventoryItem] = []
    
    func getAll(completion: @escaping (Result<[LocalInventoryItem], Error>) -> Void) {
        completion(.success(items))
    }
    
    func get(objectId: String) -> LocalInventoryItem? {
        items.first(where: { $0.id == objectId })
    }
    
    func getSoldItems() -> [LocalInventoryItem] {
        
    }
}
