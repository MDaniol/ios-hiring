//
//  DetailsPresenterTests.swift
//  HelloTreelineTests
//
//  Copyright © 2021 Treeline. All rights reserved.
//

import XCTest
@testable import HelloTreeline

class DetailsPresenterTests: XCTestCase {
    
    private var presenter: DetailsPresenter!
    private var view: MockViewController!
    
    private let inventoryItem  = InventoryItem(id: "test-id", type: "type", title: "title", description: "description", color: "color", available: 100)
    
    override func setUpWithError() throws {
        
        self.view = MockViewController()
        
        let itemRepository = MockInventoryItemRepository()
        itemRepository.items = [LocalInventoryItem(with: inventoryItem, sold: 1)]
        let interactor = DetailsInteractor(itemRepository: itemRepository)
        self.presenter = DetailsPresenter(view: view, interactor: interactor, router: StubRouter(), objectId: inventoryItem.id)
    }

    func testViewDidLoadToPopulateAllLabels() throws {
        
        // before
        XCTAssertNil(view.navBarTitle)
        XCTAssertNil(view.idLabelTitle)
        XCTAssertNil(view.titleLabelTitle)
        XCTAssertNil(view.desciptionLabelTitle)
        XCTAssertNil(view.colorLabelTitle)
        XCTAssertNil(view.itemsAvailableLabelTitle)
        XCTAssertNil(view.itemsSoldLabelTitle)
        
        // when
        presenter.viewDidLoad()
        
        // then
        XCTAssertEqual(view.navBarTitle, "\(inventoryItem.title)")
        XCTAssertEqual(view.idLabelTitle, "ID: \(inventoryItem.id)")
        XCTAssertEqual(view.titleLabelTitle, "Title: \(inventoryItem.title)")
        XCTAssertEqual(view.desciptionLabelTitle, "Description: \(inventoryItem.description)")
        XCTAssertEqual(view.colorLabelTitle, "Color: \(inventoryItem.color)")
        XCTAssertEqual(view.itemsAvailableLabelTitle, "Items available: \(inventoryItem.available)")
        XCTAssertEqual(view.itemsSoldLabelTitle, "Items sold: \("1")")
    }
    
    func testAddingItemSellLogic() {
        presenter.viewDidLoad()
        presenter.addSellOfItem()
        XCTAssertEqual(view.itemsSoldLabelTitle, "Items sold: \(2)")
    }
    
    func testSubstractingItemSellLogic() {
        presenter.viewDidLoad()
        presenter.substractSellOfItem()
        XCTAssertEqual(view.itemsSoldLabelTitle, "Items sold: \(0)")
    }
}

fileprivate class MockViewController: DetailsViewController {
    
    var navBarTitle: String?
    
    var idLabelTitle: String?
    
    var titleLabelTitle: String?
    
    var desciptionLabelTitle: String?
    
    var colorLabelTitle: String?
    
    var itemsAvailableLabelTitle: String?
    
    var itemsSoldLabelTitle: String?
}

fileprivate class StubRouter: DetailsRouter { }
